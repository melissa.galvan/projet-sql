package proj.simplon.co.promo16.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import proj.simplon.co.promo16.entity.Patient;

public class PatientRepo implements IRepository<Patient> {
    private Connection connection;

    public PatientRepo() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/allodocdb");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Patient> findAll() {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM patient");
            ResultSet result = stmt.executeQuery();
            List<Patient> patientList = new ArrayList<>();
            while (result.next()) {
                Patient patient = new Patient(
                        result.getString("phoneNumber"),
                        result.getInt("patient_id"),
                        result.getString("first_name"),
                        result.getString("last_name"));
                patientList.add(patient);
            }
            return patientList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean save(Patient entity) {
        try {
            if (entity.getId() != null){
                return update(entity);
            }
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO patient (phoneNumber, first_name, last_name) VALUES (?,?,?)",
            PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, entity.getPhoneNumber());
            stmt.setString(2, entity.getFirstName());
            stmt.setString(3, entity.getLastName());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.setId(result.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Patient findById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM patient WHERE patient_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if(result.next()) {
                return new Patient(
                    result.getString("phoneNumber"),
                    result.getInt("patient_id"),
                    result.getString("first_name"),
                    result.getString("last_name"));                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean update(Patient entity) {
        try {
            PreparedStatement stmt = connection
            .prepareStatement("UPDATE patient SET phoneNumber=?, first_name=?, last_name=? WHERE patient_id=?");
            stmt.setString(1, entity.getPhoneNumber());
            stmt.setString(2, entity.getFirstName());
            stmt.setString(3, entity.getLastName());
            stmt.setInt(4, entity.getId());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM patient WHERE patient_id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
