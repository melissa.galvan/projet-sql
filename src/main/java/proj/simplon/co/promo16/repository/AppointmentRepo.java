package proj.simplon.co.promo16.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import proj.simplon.co.promo16.entity.Appointment;

public class AppointmentRepo implements IRepository<Appointment> {
    private Connection connection;

    public AppointmentRepo() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/allodocdb");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Appointment> findAll() {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM appointment");
            ResultSet result = stmt.executeQuery();
            List<Appointment> appointmentList = new ArrayList<>();
            while (result.next()) {
                Appointment appointment = new Appointment(
                        result.getInt("appointment_id"),
                        result.getString("title"),
                        result.getString("description"),
                        result.getDate("date").toLocalDate(),
                        result.getTime("time").toLocalTime());
                appointmentList.add(appointment);
            }
            return appointmentList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Appointment findById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM appointment WHERE appointment_id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Appointment(result.getInt("appointment_id"),
                        result.getString("title"),
                        result.getString("description"),
                        result.getDate("date").toLocalDate(),
                        result.getTime("time").toLocalTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean save(Appointment appointment) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO appointment (title, description, date, time) VALUES (?, ?, ?, ?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, appointment.getTitle());
            stmt.setString(2, appointment.getDescription());
            stmt.setDate(3, Date.valueOf(appointment.getDate()));
            stmt.setTime(4, Time.valueOf(appointment.getTime()));

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                appointment.setId(result.getInt(1));

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Appointment appointment) {
        try {
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "UPDATE appointment SET title=?, description=?, date=?, time=? WHERE appointment_id=?");
            stmt.setString(1, appointment.getTitle());
            stmt.setString(2, appointment.getDescription());
            stmt.setDate(3, Date.valueOf(appointment.getDate()));
            stmt.setTime(4, Time.valueOf(appointment.getTime()));
            stmt.setInt(5, appointment.getId());
            return stmt.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM appointment WHERE appointment_id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
