package proj.simplon.co.promo16;

// import proj.simplon.co.promo16.entity.Appointment;
// import proj.simplon.co.promo16.entity.Medecin;
// import proj.simplon.co.promo16.entity.Patient;
// import proj.simplon.co.promo16.repository.AppointmentRepo;
// import proj.simplon.co.promo16.repository.IRepository;
// import proj.simplon.co.promo16.repository.MedecinRepo;
// import proj.simplon.co.promo16.repository.PatientRepo;

public class App {
    public static void main(String[] args) {
        // IRepository<Patient> patientRepo = new PatientRepo();


        // //test findAll et findByID
        // System.out.println(repo.findAll());
        // System.out.println(repo.findById(1));

        // //test save
        // Patient patient2 = new Patient("0612345678", "Melissa", "Galvan");
        // repo.save(patient2);
        // System.out.println(repo.findAll());

        // //test update
        // Patient patient3 = new Patient("0601020304", "Harry", "Cot");
        // repo.save(patient3);
        // System.out.println(repo.findAll());
        // Patient update1 = new Patient("0645253652", 3, "Yousra", "Ben Kheira");
        // repo.update(update1);
        // System.out.println(repo.findAll());

        // //test delete
        // Patient patient4 = new Patient("0601020304", "Harry", "Cot");
        // repo.save(patient4);
        // System.out.println(repo.findAll());
        // repo.deleteById(4);
        // System.out.println(repo.findAll());

        //Test medecinRepo
        // IRepository<Medecin> medecinRepo = new MedecinRepo();
        // System.out.println(repo.findById(1));
        // Medecin medecin3 = new Medecin("Sergent", "Robert");

        // medecinRepo.save(medecin3);
        // System.out.println(repo.findById());

        // Medecin medecin4 = new Medecin(3, "update", "update");
        // medecinRepo.update(medecin4);
        // System.out.println(medecinRepo.findAll());

        // medecinRepo.deleteById(4);
        // System.out.println(repo.findAll());

        // Test appointmentRepo
        // IRepository<Appointment> appointmentRepo = new AppointmentRepo();
        // save()
        // Appointment appointment1 = new Appointment("Pneumologie", "Bronchites
        // récurantes", LocalDate.of(2022, 02, 19), LocalTime.of(17, 30, 00));
        // appointmentRepo.save(appointment1);

        // update()
        // Appointment appointment2 = new Appointment(5, "Consultation générale",
        // "Migraines", LocalDate.of(2021, 06, 18), LocalTime.of(16, 40, 00));
        // appointmentRepo.update(appointment2);

        // findAll()
        // System.out.println(appointmentRepo.findAll());

        // findById()
        // System.out.println(appointmentRepo.findById(5));

        // deleteById()
        // System.out.println(appointmentRepo.deleteById(7));

    }

}
